import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Scanner;

public class LevelData {


	private int width;
	private int height;
	
	private ArrayList<String> dataLines;
	
	private int tileset;
	
	/*
		Lines that can be in a level file:
		Animation definitions (wait on that)
		Animation instances (...maybe? do definitions first)
		Items
		Weapons
		Enemies
		Doors
		Look texts
		Triggers (that'll be Very Complicated. but important)
	*/
	
	private byte[][] imageData;
	private byte[][] statusData;
	
	public LevelData(String levelName) throws IOException{
		// load image and status data
		Scanner input = new Scanner(new FileInputStream(MorlequariatLevelEditor.MORLEQUARIPATH + "/map/data/" + levelName));
		
		width = input.nextInt();
		height = input.nextInt();
		tileset = input.nextInt();
		
		dataLines = new ArrayList<>();
		while(input.hasNextLine()) dataLines.add(input.nextLine());
		
		FileInputStream imageFile = new FileInputStream(MorlequariatLevelEditor.MORLEQUARIPATH + "/map/tiles/" + levelName);
		FileInputStream statusFile = new FileInputStream(MorlequariatLevelEditor.MORLEQUARIPATH + "/map/status/" + levelName);
		
		imageData = new byte[height][];
		statusData = new byte[height][];
		for(int i=0; i<height; ++i){
			imageData[i] = new byte[width];
			statusData[i] = new byte[width];
			
			imageFile.read(imageData[i]);
			statusFile.read(statusData[i]);
		}
		
		imageFile.close();
		statusFile.close();
		input.close();
	}
	
	public byte getImage(int x, int y){
		if(x<0 || y<0 || y>=imageData.length || x>=imageData[y].length) return 0;
		return imageData[y][x];
	}
	
	public void setImage(int x, int y, byte image){
		imageData[y][x] = image;
	}
	
	public byte getStatus(int x, int y){
		return statusData[y][x];
	}
	
	public void setStatus(int x, int y, byte status){
		statusData[y][x] = status;
	}
	
	public byte[][] getTiles(){
		return imageData;
	}
	
	public byte[][] getStatus(){
		return statusData;
	}
	
	public int getWidth(){return width;}
	public int getHeight(){return height;}
	public int getTileset(){return tileset;}
	public void setTileset(int t){tileset=t;}
	
	public void resize(int w, int h) {
		byte[][] newImageData = new byte[h][];
		byte[][] newStatusData = new byte[h][];
		for(int i=0; i<Math.min(h, height); ++i) {
			newImageData[i] = new byte[w];
			newStatusData[i] = new byte[w];
			for(int j=0; j<Math.min(w, width); ++j) {
				newImageData[i][j] = imageData[i][j];
				newStatusData[i][j] = statusData[i][j];
			}
		}
		if(h > height) {
			for(int i=height; i<h; ++i) {
			newImageData[i] = new byte[w];
			newStatusData[i] = new byte[w];
			}
		}
		
		imageData = newImageData;
		statusData = newStatusData;
		height = h;
		width = w;
		width = w;
	}

}

import java.awt.Frame;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CreateLevelDialog implements ActionListener {

	private JDialog dialog;

	private JTextField levelIdField;
	private JTextField heightField;
	private JTextField widthField;
	private JTextField tilesetField;
	
	private JButton cancel;
	private JButton create;
	
	public void happen(Frame parentFrame) {
		dialog = new JDialog(parentFrame, "Create Level");
		
		dialog.setLayout(new FlowLayout());
		
		dialog.add(new JLabel("Level ID:"));
		levelIdField = new JTextField(3);
		dialog.add(levelIdField);
		
		dialog.add(new JLabel("Width:"));
		widthField = new JTextField(4);
		dialog.add(widthField);		
		
		dialog.add(new JLabel("Height:"));
		heightField = new JTextField(4);
		dialog.add(heightField);		
		
		dialog.add(new JLabel("Tileset:"));
		tilesetField = new JTextField(2);
		dialog.add(tilesetField);
		
		create = new JButton("Create");
		create.addActionListener(this);
		dialog.add(create);
		
		cancel = new JButton("Cancel");
		cancel.addActionListener(this);
		dialog.add(cancel);
		
		dialog.pack();
		dialog.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource() == cancel) dialog.dispose();
		else {
			String levelId = levelIdField.getText();
			int width, height, tileset;
						
			try {
				width = Integer.parseInt(widthField.getText());
			} catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(dialog, "Width must be an integer");
				return;
			}
		
			try {
				height = Integer.parseInt(heightField.getText());
			} catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(dialog, "Height must be an integer");
				return;
			}
			
			try {
				tileset = Integer.parseInt(tilesetField.getText());
			} catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(dialog, "Tileset must be an integer");
				return;
			}
			
			File dataFile = new File(MorlequariatLevelEditor.MORLEQUARIPATH + "/map/data/" + levelId);
			if(dataFile.exists()){
				JOptionPane.showMessageDialog(dialog, "Level metadata file already exists");
				return;
			}
			
			File imagesFile = new File(MorlequariatLevelEditor.MORLEQUARIPATH + "/map/tiles/" + levelId);
			if(imagesFile.exists()){
				JOptionPane.showMessageDialog(dialog, "Level image tiles file already exists");
				return;
			}
			File statusFile = new File(MorlequariatLevelEditor.MORLEQUARIPATH + "/map/status/" + levelId);
			if(statusFile.exists()){
				JOptionPane.showMessageDialog(dialog, "Level tile status file already exists");
				return;
			}
			
			try {
				dataFile.createNewFile();
				imagesFile.createNewFile();
				statusFile.createNewFile();
				
				FileWriter dataWriter = new FileWriter(dataFile);
				dataWriter.write(String.format("%d %d %d", width, height, tileset));
				dataWriter.close();
				
				byte[] bytes = new byte[width*height]; // initialized with zeros
				
				FileOutputStream imagesFos = new FileOutputStream(imagesFile);
				imagesFos.write(bytes);
				imagesFos.close();
				
				FileOutputStream statusFos = new FileOutputStream(statusFile);
				statusFos.write(bytes);
				statusFos.close();
			} catch(IOException e) {
				e.printStackTrace();
				return;
			}
			
			MorlequariatLevelEditor.loadLevel(levelId);
			dialog.dispose();
		}
	}

}
import java.awt.Image;
import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class NumericImageCellRenderer extends DefaultListCellRenderer {
	
	private ImageIcon[] imageIcons;
	
	public NumericImageCellRenderer(){
		imageIcons = new ImageIcon[0];
	}
	
	public NumericImageCellRenderer(Image[] images){
		setImageList(images);
	}
	
	public void setImageList(Image[] images){
		imageIcons = new ImageIcon[images.length];
		for(int i=0; i<images.length; ++i) imageIcons[i] = new ImageIcon(images[i]);
	}
	
	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean selected, boolean focus) {
		JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, selected, focus);
		
		if(index < imageIcons.length) {
			label.setIcon(imageIcons[index]);
			label.setIconTextGap(-imageIcons[index].getIconWidth());
			label.setText(Integer.toHexString(index));
			label.setForeground(Color.WHITE);
			//label.setBackground(Color.BLACK);
		}
		
		return label;
	}
}
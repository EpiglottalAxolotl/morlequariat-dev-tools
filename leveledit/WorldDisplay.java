import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseAdapter;
import java.io.File;
import java.io.IOException;
import javax.swing.JList;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class WorldDisplay extends Component implements ListSelectionListener {

	public static final int TILE_SIZE = MorlequariatLevelEditor.TILE_SIZE;
	public static final int SCREENW = 16;
	public static final int SCREENH = 16;
	
	private int offsetX;
	private int offsetY;
	
	private int cursorX;
	private int cursorY;
	private boolean isCursor;
	
	private int selectionOriginX;
	private int selectionOriginY;
	private int selectionEndX;
	private int selectionEndY;
	
	private int[][] imageClipboard;
	private int[][] statusClipboard;
	
	private Image osc;
	
	private int activeBrush;
	
	private boolean editStatus;
	private boolean showBoth;
		
	public WorldDisplay() {
		osc = new BufferedImage(SCREENW*TILE_SIZE, SCREENH*TILE_SIZE, BufferedImage.TYPE_INT_RGB);
		setFocusable(true);
		addKeyListener(new KeyboardInput());
		MouseInput mouseInput = new MouseInput();
		addMouseListener(mouseInput);
		addMouseMotionListener(mouseInput);
		addMouseWheelListener(mouseInput);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(SCREENW*TILE_SIZE,SCREENH*TILE_SIZE);
	}
	
	private boolean doesNotInterpolate(int baseTile, int questioned) {
		return (questioned & 0xE6) != ((baseTile & 0x06) | 0xE0);
	}
	
	private void setInterpolatedTile(int x, int y, int t, LevelData data, Image[] interp, Graphics osg) {
		int screenX = (x - offsetX) * TILE_SIZE;
		int screenY = (y - offsetY) * TILE_SIZE;
		
		if (y > 0 && doesNotInterpolate(t, data.getImage(x, y-1))) {
			
			osg.drawImage(interp[0 * 4 + 1], screenX + (1 * TILE_SIZE / 4), screenY + (0 * TILE_SIZE / 4), null);
			osg.drawImage(interp[0 * 4 + 2], screenX + (2 * TILE_SIZE / 4), screenY + (0 * TILE_SIZE / 4), null);
		
			if (x > 0 && doesNotInterpolate(t, data.getImage(x-1, y))) {
				osg.drawImage(interp[0 * 4 + 0], screenX + (0 * TILE_SIZE / 4), screenY + (0 * TILE_SIZE / 4), null);
				osg.drawImage(interp[1 * 4 + 0], screenX + (0 * TILE_SIZE / 4), screenY + (1 * TILE_SIZE / 4), null);
				osg.drawImage(interp[2 * 4 + 0], screenX + (0 * TILE_SIZE / 4), screenY + (2 * TILE_SIZE / 4), null);
			} else {
				osg.drawImage(interp[0 * 4 + 1], screenX + (0 * TILE_SIZE / 4), screenY + (0 * TILE_SIZE / 4), null);
			}
			
			if (x < data.getWidth() && doesNotInterpolate(t, data.getImage(x+1, y))) {
				osg.drawImage(interp[0 * 4 + 3], screenX + (3 * TILE_SIZE / 4), screenY + (0 * TILE_SIZE / 4), null);
				osg.drawImage(interp[1 * 4 + 3], screenX + (3 * TILE_SIZE / 4), screenY + (1 * TILE_SIZE / 4), null);
				osg.drawImage(interp[2 * 4 + 3], screenX + (3 * TILE_SIZE / 4), screenY + (2 * TILE_SIZE / 4), null);
			} else {
				osg.drawImage(interp[0 * 4 + 2], screenX + (3 * TILE_SIZE / 4), screenY + (0 * TILE_SIZE / 4), null);
			}
			
		} else {
			if (x > 0 && doesNotInterpolate(t, data.getImage(x-1, y))) {
				osg.drawImage(interp[1 * 4 + 0], screenX + (0 * TILE_SIZE / 4), screenY + (0 * TILE_SIZE / 4), null);
				osg.drawImage(interp[1 * 4 + 0], screenX + (0 * TILE_SIZE / 4), screenY + (1 * TILE_SIZE / 4), null);
				osg.drawImage(interp[2 * 4 + 0], screenX + (0 * TILE_SIZE / 4), screenY + (2 * TILE_SIZE / 4), null);
			} else if(x > 0 && y > 0 && doesNotInterpolate(t, data.getImage(x-1, y-1))) {
				osg.drawImage(interp[1 * 4 + 1], screenX + (0 * TILE_SIZE / 4), screenY + (0 * TILE_SIZE / 4), null);
			}
			if (x < data.getWidth() && doesNotInterpolate(t, data.getImage(x+1, y))) {
				osg.drawImage(interp[1 * 4 + 3], screenX + (3 * TILE_SIZE / 4), screenY + (0 * TILE_SIZE / 4), null);
				osg.drawImage(interp[1 * 4 + 3], screenX + (3 * TILE_SIZE / 4), screenY + (1 * TILE_SIZE / 4), null);
				osg.drawImage(interp[2 * 4 + 3], screenX + (3 * TILE_SIZE / 4), screenY + (2 * TILE_SIZE / 4), null);
			} else if(x < data.getWidth() && y > 0 && doesNotInterpolate(t, data.getImage(x+1, y-1))) {
				osg.drawImage(interp[1 * 4 + 2], screenX + (3 * TILE_SIZE / 4), screenY + (0 * TILE_SIZE / 4), null);
			}
		}
		
		if (y < data.getHeight() && doesNotInterpolate(t, data.getImage(x, y+1))) {
			osg.drawImage(interp[3 * 4 + 1], screenX + (1 * TILE_SIZE / 4), screenY + (3 * TILE_SIZE / 4), null);
			osg.drawImage(interp[3 * 4 + 2], screenX + (2 * TILE_SIZE / 4), screenY + (3 * TILE_SIZE / 4), null);
			
			if (x > 0 && doesNotInterpolate(t, data.getImage(x-1, y))) {
				osg.drawImage(interp[3 * 4 + 0], screenX + (0 * TILE_SIZE / 4), screenY + (3 * TILE_SIZE / 4), null);
			} else {
				osg.drawImage(interp[3 * 4 + 1], screenX + (0 * TILE_SIZE / 4), screenY + (3 * TILE_SIZE / 4), null);
			}
			if (x < data.getWidth() && doesNotInterpolate(t, data.getImage(x+1, y))) {
				osg.drawImage(interp[3 * 4 + 3], screenX + (3 * TILE_SIZE / 4), screenY + (3 * TILE_SIZE / 4), null);
			} else {
				osg.drawImage(interp[3 * 4 + 2], screenX + (3 * TILE_SIZE / 4), screenY + (3 * TILE_SIZE / 4), null);
			}
		} else {
			if (x > 0 && doesNotInterpolate(t, data.getImage(x-1, y))) {
				osg.drawImage(interp[2 * 4 + 0], screenX + (0 * TILE_SIZE / 4), screenY + (3 * TILE_SIZE / 4), null);
			} else if(x > 0 && y < data.getHeight() && doesNotInterpolate(t, data.getImage(x-1, y+1))) {
				osg.drawImage(interp[2 * 4 + 1], screenX + (0 * TILE_SIZE / 4), screenY + (3 * TILE_SIZE / 4), null);
			}
			if (x < data.getWidth() && doesNotInterpolate(t, data.getImage(x+1, y))) {
				osg.drawImage(interp[2 * 4 + 3], screenX + (3 * TILE_SIZE / 4), screenY + (3 * TILE_SIZE / 4), null);
			} else if(x < data.getWidth() && y < data.getHeight() && doesNotInterpolate(t, data.getImage(x+1 + offsetX, y+1 + offsetY))) {
				osg.drawImage(interp[2 * 4 + 2], screenX + (3 * TILE_SIZE / 4), screenY + (3 * TILE_SIZE / 4), null);
			}
		}
	}

	@Override
	public void paint(Graphics g){
		Graphics osg = osc.getGraphics();
		osg.clearRect(0,0,SCREENW*TILE_SIZE, SCREENH*TILE_SIZE);
		LevelData data = MorlequariatLevelEditor.getData();
		if(data == null) {
			osg.drawString("No data loaded", SCREENW*TILE_SIZE/2, SCREENH*TILE_SIZE/2);
			g.drawImage(osc, 0, 0, null);
			return;
		}
		int xmax = Math.min(SCREENW, data.getWidth()-offsetX);
		int ymax = Math.min(SCREENH, data.getHeight()-offsetY);
		int xmin = Math.max(0, -offsetX);
		int ymin = Math.max(0, -offsetY);
		
		Image[] tiles = MorlequariatLevelEditor.getTileset();
		Image[] statusSymbols = MorlequariatLevelEditor.getStatusSymbols();
		Image[][] interp = MorlequariatLevelEditor.getInterpolatedFrames();
		
		if(showBoth || !editStatus) {
			for(int i=xmin; i<xmax; ++i){
				for(int j=ymin; j<ymax; ++j){
					int t = data.getImage(i+offsetX, j+offsetY) & 0xff;
					if( (t & 0xF1) == 0xF1 ) {
						osg.drawImage(tiles[t - 1], i*TILE_SIZE, j*TILE_SIZE, null);
						setInterpolatedTile(i+offsetX, j+offsetY, t, data, interp[(t - 0xF0)/2], osg);
					} else {
						osg.drawImage(tiles[t], i*TILE_SIZE, j*TILE_SIZE, null);
					}
				}
			}
		}
		
		if(showBoth || editStatus) {
			for(int i=xmin; i<xmax; ++i){
				for(int j=ymin; j<ymax; ++j){
					osg.drawImage(statusSymbols[data.getStatus(i+offsetX, j+offsetY) & 0xff], i*TILE_SIZE, j*TILE_SIZE, null);
				}
			}
		}
		
		if(isCursor){
			osg.drawImage( (editStatus ? statusSymbols : tiles) [activeBrush], cursorX * TILE_SIZE, cursorY * TILE_SIZE, null);
			osg.drawRect(cursorX * TILE_SIZE, cursorY * TILE_SIZE, TILE_SIZE, TILE_SIZE);
		}
		
		if(selectionOriginX >= 0) {
			osg.drawRect(
					(Math.min(selectionOriginX, selectionEndX) - offsetX) * TILE_SIZE,
					(Math.min(selectionOriginY, selectionEndY) - offsetY) * TILE_SIZE,
					(Math.abs(selectionOriginX - selectionEndX)) * TILE_SIZE,
					(Math.abs(selectionOriginY - selectionEndY)) * TILE_SIZE
			);
		}
		g.drawImage(osc, 0, 0, null);
	}
	
	public void setEditStatus(boolean e) { editStatus = e; repaint(); }
	public void setShowBoth(boolean e) { showBoth = e; repaint(); }
	
	public void selectAll() {
		selectionOriginX = 0;
		selectionOriginY = 0;
		selectionEndX = MorlequariatLevelEditor.getData().getWidth();
		selectionEndY = MorlequariatLevelEditor.getData().getHeight();
	}
	
	public void fillSelection() {
		if(selectionOriginX < 0) return;
		for(int i=Math.min(selectionOriginX, selectionEndX); i<Math.max(selectionOriginX, selectionEndX); ++i) {
			for(int j=Math.min(selectionOriginY, selectionEndY); j<Math.max(selectionOriginY, selectionEndY); ++j) {
				MorlequariatLevelEditor.setTile(i, j, editStatus, (byte)activeBrush);
			}
		}
		repaint();
	}
	
	public void copy() {
		if(selectionOriginX < 0) return;
		
		int xmin = Math.min(selectionOriginX, selectionEndX);
		int ymin = Math.min(selectionOriginY, selectionEndY);
		int xmax = Math.max(selectionOriginX, selectionEndX);
		int ymax = Math.max(selectionOriginY, selectionEndY);
		
		imageClipboard = new int[xmax-xmin][ymax-ymin];
		statusClipboard = new int[xmax-xmin][ymax-ymin];
		
		for(int i=0; i<xmax-xmin; ++i) {
			for(int j=0; j<ymax-ymin; ++j) {
				imageClipboard[i][j] = MorlequariatLevelEditor.getData().getImage(xmin+i, ymin+j) & 0xff;
				statusClipboard[i][j] = MorlequariatLevelEditor.getData().getStatus(xmin+i, ymin+j) & 0xff;
			}
		}
	}
	
	public void paste() {
		if(imageClipboard == null) return;
		
		int xmin = Math.max(0, cursorX + offsetX);
		int ymin = Math.max(0, cursorY + offsetY);
		int xmax = Math.min(MorlequariatLevelEditor.getData().getWidth(), cursorX + offsetX + imageClipboard.length);
		int ymax = Math.min(MorlequariatLevelEditor.getData().getHeight(), cursorY + offsetY + imageClipboard[0].length);
						
		for(int i=0; i<xmax-xmin; ++i) {
			for(int j=0; j<ymax-ymin; ++j) {
				MorlequariatLevelEditor.setTile(xmin+i, ymin+j, true, (byte) statusClipboard[i][j]);
				MorlequariatLevelEditor.setTile(xmin+i, ymin+j, false, (byte) imageClipboard[i][j]);
			}
		}
		
		repaint();
	}
	
	public void flipH() {
		if(selectionOriginX < 0) return;		
		
		int xmin = Math.min(selectionOriginX, selectionEndX);
		int ymin = Math.min(selectionOriginY, selectionEndY);
		int xmax = Math.max(selectionOriginX, selectionEndX);
		int ymax = Math.max(selectionOriginY, selectionEndY);
		
		/*if (xmin < 0 || ymin < 0 || xmin == xmax || ymin == ymax) {
			// no selection or empty selection
			xmin = 0;
			ymin = 0;
			xmax = SCREENW
		}*/
		
		int[][] imageTemp = new int[xmax-xmin][ymax-ymin];
		int[][] statusTemp = new int[xmax-xmin][ymax-ymin];
		
		for(int j=0; j<ymax-ymin; ++j) {
			for(int i=0; i<xmax-xmin; ++i) {
				imageTemp[i][j] = MorlequariatLevelEditor.getData().getImage(xmin+i, ymin+j) & 0xff;
				statusTemp[i][j] = MorlequariatLevelEditor.getData().getStatus(xmin+i, ymin+j) & 0xff;
			}
			for(int i=0; i<xmax-xmin; ++i) {
				MorlequariatLevelEditor.setTile(xmax-1-i, ymin+j, true, (byte) statusTemp[i][j]);
				MorlequariatLevelEditor.setTile(xmax-1-i, ymin+j, false, (byte) imageTemp[i][j]);
			}
		}
		
		repaint();
	}
	
	public void flipV() {
		if(selectionOriginX < 0) return;		
		
		int xmin = Math.min(selectionOriginX, selectionEndX);
		int ymin = Math.min(selectionOriginY, selectionEndY);
		int xmax = Math.max(selectionOriginX, selectionEndX);
		int ymax = Math.max(selectionOriginY, selectionEndY);
				
		int[][] imageTemp = new int[xmax-xmin][ymax-ymin];
		int[][] statusTemp = new int[xmax-xmin][ymax-ymin];
		
		for(int i=0; i<xmax-xmin; ++i) {
			for(int j=0; j<ymax-ymin; ++j) {
				imageTemp[i][j] = MorlequariatLevelEditor.getData().getImage(xmin+i, ymin+j) & 0xff;
				statusTemp[i][j] = MorlequariatLevelEditor.getData().getStatus(xmin+i, ymin+j) & 0xff;
			}
			for(int j=0; j<ymax-ymin; ++j) {
				MorlequariatLevelEditor.setTile(xmin+i, ymax-1-j, true, (byte) statusTemp[i][j]);
				MorlequariatLevelEditor.setTile(xmin+i, ymax-1-j, false, (byte) imageTemp[i][j]);
			}
		}
		
		repaint();
	}
	
	public class KeyboardInput implements KeyListener {
		///private final HashSet<Integer> pressed = new HashSet<Integer>();// Set of currently pressed keys
		
		@Override
		public synchronized void keyPressed(KeyEvent e) {
			switch(e.getKeyCode()){
				case KeyEvent.VK_W:
					if(e.isShiftDown()) offsetY = 0;
					else offsetY -= 1;
					break;
				case KeyEvent.VK_A:
					if(e.isShiftDown()) offsetX = 0;
					else offsetX -= 1;
					break;
				case KeyEvent.VK_S:
					if(e.isShiftDown()) offsetY = MorlequariatLevelEditor.getData().getHeight() - SCREENH;
					else offsetY += 1;
					break;
				case KeyEvent.VK_D:
					if(e.isShiftDown()) offsetX = MorlequariatLevelEditor.getData().getWidth() - SCREENW;
					else offsetX += 1;
					break;
				case KeyEvent.VK_F:
					fillSelection();
					break;
				case KeyEvent.VK_C:
					copy();
					break;
				case KeyEvent.VK_V:
					paste();
					break;
			}
			repaint();
		}
		@Override
		public synchronized void keyReleased(KeyEvent e) {
			//pressed.remove(e.getKeyCode());
		}
		
		@Override
		public void keyTyped(KeyEvent e){
			// not necessary
		}
	}	
	
	@Override
	public void valueChanged(ListSelectionEvent e) {
		activeBrush = ((JList) e.getSource()).getSelectedIndex();
	}
	
	public class MouseInput extends MouseAdapter {
			@Override
			public void mouseClicked(MouseEvent e) {
				cursorX = e.getX() / TILE_SIZE;
				cursorY = e.getY() / TILE_SIZE;
				if(e.getButton() == MouseEvent.BUTTON1) {
					MorlequariatLevelEditor.setTile(cursorX+offsetX, cursorY+offsetY, editStatus, (byte)activeBrush);
				} else {
					if(editStatus) activeBrush = MorlequariatLevelEditor.getData().getStatus(cursorX+offsetX, cursorY+offsetY) & 0xff;
					else activeBrush = MorlequariatLevelEditor.getData().getImage(cursorX+offsetX, cursorY+offsetY) & 0xff;
				}
				repaint();
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				requestFocusInWindow();
				isCursor = true;
				repaint();
			}
			@Override
			public void mouseExited(MouseEvent e) {
				isCursor = false;
				repaint();
			}
			@Override
			public void mousePressed(MouseEvent e) {
				if(SwingUtilities.isRightMouseButton(e)){
					selectionOriginX = cursorX+offsetX;
					selectionOriginY = cursorY+offsetY;
					
					selectionEndX = cursorX+offsetX;
					selectionEndY = cursorY+offsetY;
				} else {
					selectionOriginX = -1;
					selectionOriginY = -1;
				}
			}
			@Override
			public void mouseReleased(MouseEvent e) {
			}
			@Override
			public void mouseDragged(MouseEvent e) {
				
				if(SwingUtilities.isMiddleMouseButton(e)){
					offsetX += cursorX - e.getX() / TILE_SIZE;
					offsetY += cursorY - e.getY() / TILE_SIZE;
				}
				
				cursorX = e.getX() / TILE_SIZE;
				cursorY = e.getY() / TILE_SIZE;
				
				if(SwingUtilities.isLeftMouseButton(e)){
					MorlequariatLevelEditor.setTile(cursorX+offsetX, cursorY+offsetY, editStatus, (byte)activeBrush);
				} else if(SwingUtilities.isRightMouseButton(e) && cursorX + offsetX >= 0 && cursorY + offsetY >= 0 && cursorX + offsetX <= MorlequariatLevelEditor.getData().getWidth() && cursorY + offsetY <= MorlequariatLevelEditor.getData().getHeight()) {
					selectionEndX = cursorX+offsetX;
					selectionEndY = cursorY+offsetY;
				}
				repaint();
			}
			@Override
			public void mouseMoved(MouseEvent e) {
				cursorX = e.getX() / TILE_SIZE;
				cursorY = e.getY() / TILE_SIZE;
				MorlequariatLevelEditor.setCursorPosition(cursorX+offsetX, cursorY+offsetY);
				repaint();
			}
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				activeBrush = (activeBrush - e.getWheelRotation() + 256) % 256;
				repaint();
			}
	}
}
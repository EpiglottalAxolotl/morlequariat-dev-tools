Click to paint using the selected tile as a brush.
Select tiles in the tiles pane or by scrolling.
Move the map with WASD, or use Shift-WASD to snap to an edge. Alternatively, click and drag with the scroll wheel to pan.
Right-click a tile to set the active brush to it.
Right-click and drag to select a rectangular area.
Press F to fill the selection.
Press C to copy the selection to the clipboard.
Press V to paste the contents of the clipboard at the cursor location.
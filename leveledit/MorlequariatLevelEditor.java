
import java.awt.Image;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JOptionPane;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ListSelectionModel;

import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

public class MorlequariatLevelEditor {

	public static final String MORLEQUARIPATH = "..\\..\\..\\Morlequariat";
	public static final int TILE_SIZE = 48;

	private static JFrame frame;
	
	private static JPanel sidePanel;
	
		private static JPanel loadPanel;	
			private static JTextField levelSelect;
			private static JButton loadButton;
		
		private static JPanel savePanel;
			private static JButton newButton;
			private static JButton saveTilesButton;
			private static JButton saveStatusButton;
			
		private static JPanel resizePanel;
			private static JTextField widthField;
			private static JTextField heightField;
			private static JButton resizeButton;
			private static JTextField tilesetField;
			private static JButton tilesetButton;
			
		private static JPanel selectionOpsPanel;
			private static JButton selectAllButton;
			private static JButton fillButton;
			private static JButton copyButton;
			private static JButton pasteButton;
			private static JButton hFlipButton;
			private static JButton vFlipButton;
			
		private static JPanel modePanel;
			private static ButtonGroup editModeButtons;
				private static JRadioButton imageModeButton;
				private static JRadioButton statusModeButton;
			private static JCheckBox showBothButton;
				
		private static JLabel cursorPositionLabel;
		
	private static JList<Integer> palette;
		private static NumericImageCellRenderer paletteRenderer;
	
	private static WorldDisplay worldDisplay;
	
	private static String level;
	private static LevelData data;
	private static boolean tilesPristine = true;
	private static boolean statusPristine = true;
	private static Image[] tileset;
	private static Image[] statusSymbols;
	private static Image[][] interpolatedFrames;
	
	public static LevelData getData(){
		return data;
	}
	
	public static Image[] getTileset(){
		return tileset;
	}
	
	public static Image[][] getInterpolatedFrames() {
		return interpolatedFrames;
	}
	
	public static Image[] getStatusSymbols(){
		return statusSymbols;
	}
	
	private static boolean confirmUnsavedChanges() {
		if(tilesPristine && statusPristine) return true;

		int response = JOptionPane.showConfirmDialog(null, "Save changes before closing?", "Unsaved changes", JOptionPane.YES_NO_CANCEL_OPTION);
		
		if(response == JOptionPane.YES_OPTION) {
			saveTiles();
			saveStatus();
		} else if (response != JOptionPane.NO_OPTION) {
			return false;
		}
		
		return true;
	}
	
	private static class ButtonListener implements ActionListener, ItemListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == loadButton || e.getSource() == levelSelect){
				if(!confirmUnsavedChanges()) return;
				loadLevel(levelSelect.getText());
			} else if(e.getSource() == saveTilesButton) {
				saveTiles();
			} else if(e.getSource() == saveStatusButton) {
				saveStatus();
			} else if(e.getSource() == newButton) {
				if(!confirmUnsavedChanges()) return;
				new CreateLevelDialog().happen(frame);
			} else if(e.getSource() == selectAllButton) {
				worldDisplay.selectAll();
			} else if(e.getSource() == fillButton) {
				worldDisplay.fillSelection();
			} else if(e.getSource() == copyButton) {
				worldDisplay.copy();
			} else if(e.getSource() == pasteButton) {
				worldDisplay.paste();
			} else if(e.getSource() == hFlipButton) {
				worldDisplay.flipH();
			} else if(e.getSource() == vFlipButton) {
				worldDisplay.flipV();
			} else if(e.getSource() == imageModeButton) {
				paletteRenderer.setImageList(tileset);
				worldDisplay.setEditStatus(false);
			} else if(e.getSource() == statusModeButton) {
				paletteRenderer.setImageList(statusSymbols);
				worldDisplay.setEditStatus(true);
			} else if(e.getSource() == resizeButton) {
				data.resize(Integer.parseInt(widthField.getText()), Integer.parseInt(heightField.getText()));
				setTilesPristine(false);
				setStatusPristine(false);
			} else if(e.getSource() == tilesetButton || e.getSource() == tilesetField) {
				int tsi = Integer.parseInt(tilesetField.getText());
				try {
					loadTileset(tsi);
				} catch(IOException ioe) {
					JOptionPane.showMessageDialog(null, "Tileset "+tsi+" could not be loaded", "Tileset load error", JOptionPane.WARNING_MESSAGE);
				}
				data.setTileset(tsi);
				if(!statusModeButton.isSelected()) paletteRenderer.setImageList(tileset);
				worldDisplay.repaint();
			}
		}
				
		@Override
		public void itemStateChanged(ItemEvent e) {
			if(e.getItemSelectable() == showBothButton) worldDisplay.setShowBoth(showBothButton.isSelected());
		}
	}
	
	public static void setTilesPristine(boolean pristine){
		if(pristine == tilesPristine) return; // don't bother re-rendering ui
		tilesPristine = pristine;
		
		saveTilesButton.setEnabled(!pristine);
	}
	
	public static void setStatusPristine(boolean pristine){
		if(pristine == statusPristine) return; // don't bother re-rendering ui
		statusPristine = pristine;
		
		saveStatusButton.setEnabled(!pristine);
	}
	
	public static void setTile(int x, int y, boolean editStatus, byte tile){
		if(x<0 || y<0 || x>=data.getWidth() || y>=data.getHeight()) return;
		if(editStatus) data.setStatus(x, y, tile);
		else data.setImage(x, y, tile);
		if(editStatus) setStatusPristine(false);
		else setTilesPristine(false);
	}
	
	public static void setCursorPosition(int x, int y){
		cursorPositionLabel.setText(String.format("Cursor: %d, %d", x, y));
	}
	
	public static void setLevel(String levelName){
		level = levelName;
		levelSelect.setText(level);
	}

	public static void loadLevel(String levelName){
		System.out.println("Loading level "+levelName);
		
		try {
			setLevel(levelName);
			data = new LevelData(levelName);
			loadTileset(data.getTileset());
			if(!statusModeButton.isSelected()) paletteRenderer.setImageList(tileset);
			
			widthField.setText(Integer.toString(data.getWidth()));
			heightField.setText(Integer.toString(data.getHeight()));
			tilesetField.setText(Integer.toString(data.getTileset()));
		} catch(IOException e) {
			JOptionPane.showMessageDialog(null, "Level "+levelName+" could not be loaded", "No such level", JOptionPane.WARNING_MESSAGE);
			return;
		}

		setTilesPristine(true);
		setStatusPristine(true);
		worldDisplay.repaint();
	}
	
	public static void saveTiles(){
		
		try {
			File outFile = new File(MORLEQUARIPATH + "/map/tiles/" + level);
			// TODO confirmation on overwriting
			FileOutputStream fos = new FileOutputStream(outFile);
			
			for(byte[] row : data.getTiles()) fos.write(row);
			
			fos.close();
		
			// Expected bug: type a level, click load, type a level, click save, second level gets overwritten
			// but maybe that's okay
			System.out.println("Saved level to "+outFile.getCanonicalPath());
			setTilesPristine(true);
			
		} catch(IOException ioe) {
			ioe.printStackTrace();
			return;
		}
			
	}
	
	public static void saveStatus(){
		
		try {
			File outFile = new File(MORLEQUARIPATH + "/map/status/" + level);
			// TODO confirmation on overwriting
			FileOutputStream fos = new FileOutputStream(outFile);
			
			for(byte[] row : data.getStatus()) fos.write(row);
			
			fos.close();
		
			// Expected bug: type a level, click load, type a level, click save, second level gets overwritten
			// but maybe that's okay
			System.out.println("Saved level to "+outFile.getCanonicalPath());
			setStatusPristine(true);
			
		} catch(IOException ioe) {
			ioe.printStackTrace();
			return;
		}
			
	}
	
	public static void loadTileset(int tilesetIndex) throws IOException {
		tileset = loadTileset(MorlequariatLevelEditor.MORLEQUARIPATH + "/img/tiles/" + tilesetIndex + ".png");
		interpolatedFrames = loadInterpolatedFrames(tilesetIndex);
	}
	
	private static Image[] loadTileset(String filepath) throws IOException {
		Image[] tiles = new Image[256];
		BufferedImage tileset = ImageIO.read(new File(filepath));
		for(int i=0; i<256; ++i){
			tiles[i] = tileset.getSubimage(i%16 * TILE_SIZE, i/16 * TILE_SIZE, TILE_SIZE, TILE_SIZE);
		}
		return tiles;
	}
	
	private static Image[][] loadInterpolatedFrames(int tilesetIndex) throws IOException {
		Image[][] interpolatedFrames = new Image[4][];
		BufferedImage tileset = ImageIO.read(new File(MorlequariatLevelEditor.MORLEQUARIPATH + "/img/tiles/" + tilesetIndex + ".png"));
		for(int j=0; j<4; ++j) {
			interpolatedFrames[j] = new Image[16];
			for(int i=0; i<16; ++i){
				interpolatedFrames[j][i] = tileset.getSubimage(j*2*TILE_SIZE + TILE_SIZE + ( i%4 * TILE_SIZE/4 ), 0x0F*TILE_SIZE + ( i/4 * TILE_SIZE/4 ), TILE_SIZE/4, TILE_SIZE/4);
			}
		}
		return interpolatedFrames;		
	}
	
	public static void main(String[] args){
		
		try{
			loadTileset(0);
			statusSymbols = loadTileset("status_symbols.png");
		}catch(IOException e){
			System.err.println("Cannot load tileset!");
			return;
		}
		
		frame = new JFrame("MorlequariEdit");
		JPanel panel = new JPanel();
		//panel.setLayout()
		frame.setContentPane(panel);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if(tilesPristine && statusPristine) e.getWindow().dispose();
				
				else {
					int response = JOptionPane.showConfirmDialog(null, "Save changes before closing?", "Unsaved changes", JOptionPane.YES_NO_CANCEL_OPTION);
					switch(response) {
						case JOptionPane.YES_OPTION:
							saveTiles();
							//save other stuff also
							e.getWindow().dispose();
							break;
							
						case JOptionPane.NO_OPTION:
							e.getWindow().dispose();
							break;
							
						default:
							break;
					}
				}
			}
		});
		
		ButtonListener listener = new ButtonListener();
		
		sidePanel = new JPanel();
			sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.PAGE_AXIS));
		
			loadPanel = new JPanel();
			
				levelSelect = new JTextField(5);
				levelSelect.addActionListener(listener);
				loadPanel.add(levelSelect);
				
				loadButton = new JButton("Load");
				loadButton.addActionListener(listener);
				loadPanel.add(loadButton);
			
				sidePanel.add(loadPanel);
				
			savePanel = new JPanel();
			
				saveTilesButton = new JButton("Save Tiles");
				saveTilesButton.addActionListener(listener);
				saveTilesButton.setEnabled(false);
				savePanel.add(saveTilesButton);
				
				saveStatusButton = new JButton("Save Status");
				saveStatusButton.addActionListener(listener);
				saveStatusButton.setEnabled(false);
				savePanel.add(saveStatusButton);
				
				newButton = new JButton("New...");
				newButton.addActionListener(listener);
				savePanel.add(newButton);
				
				sidePanel.add(savePanel);
				
			resizePanel = new JPanel();
			
				widthField = new JTextField(3);
				resizePanel.add(widthField);
				
				heightField = new JTextField(3);
				resizePanel.add(heightField);
				
				resizeButton = new JButton("Resize");
				resizeButton.addActionListener(listener);
				resizePanel.add(resizeButton);
				
				tilesetField = new JTextField(3);
				tilesetField.addActionListener(listener);
				resizePanel.add(tilesetField);
				
				tilesetButton = new JButton("Set tileset");
				tilesetButton.addActionListener(listener);
				resizePanel.add(tilesetButton);
				
				sidePanel.add(resizePanel);
				
			selectionOpsPanel = new JPanel();
				
				selectAllButton = new JButton("Select All (A)");
				selectAllButton.addActionListener(listener);
				selectionOpsPanel.add(selectAllButton);
				
				fillButton = new JButton("Fill (F)");
				fillButton.addActionListener(listener);
				selectionOpsPanel.add(fillButton);
				
				copyButton = new JButton("Copy (C)");
				copyButton.addActionListener(listener);
				selectionOpsPanel.add(copyButton);
				
				pasteButton = new JButton("Paste (V)");
				pasteButton.addActionListener(listener);
				//selectionOpsPanel.add(pasteButton); // Removing paste button, because paste requires cursor location
				
				hFlipButton = new JButton("Flip H");
				hFlipButton.addActionListener(listener);
				selectionOpsPanel.add(hFlipButton);
				
				vFlipButton = new JButton("Flip V");
				vFlipButton.addActionListener(listener);
				selectionOpsPanel.add(vFlipButton);
				
				sidePanel.add(selectionOpsPanel);

			modePanel = new JPanel();
				
				editModeButtons = new ButtonGroup();
				
					imageModeButton = new JRadioButton("Edit images");
					imageModeButton.setActionCommand("image");
					imageModeButton.setSelected(true);
					imageModeButton.addActionListener(listener);
					modePanel.add(imageModeButton);
					editModeButtons.add(imageModeButton);
					
					statusModeButton = new JRadioButton("Edit statuses");
					statusModeButton.setActionCommand("status");
					statusModeButton.addActionListener(listener);
					modePanel.add(statusModeButton);
					editModeButtons.add(statusModeButton);
					
				showBothButton = new JCheckBox("Show both");
				showBothButton.addItemListener(listener);
				modePanel.add(showBothButton);
				
				sidePanel.add(modePanel);
		
			/*miscNumbersPanel = new JPanel();
			
				tilesetField = new JTextField(3);
				changeTilesetButton = new JButton("Set Tileset");
			
				musicIdField = new JTextField(3);
				miscNumbersPanel.add*/
		
			cursorPositionLabel = new JLabel("Cursor:");
			sidePanel.add(cursorPositionLabel);
		
			panel.add(sidePanel);
			
		worldDisplay = new WorldDisplay();
			
		Integer[] ints = new Integer[256];
		for(int i=0;i<256;++i) ints[i]=i;
		palette = new JList<Integer>(ints);
			palette.setLayoutOrientation(JList.HORIZONTAL_WRAP);
			palette.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			paletteRenderer = new NumericImageCellRenderer(tileset);
			palette.setCellRenderer(paletteRenderer);
			palette.setVisibleRowCount(-1);
			
			palette.addListSelectionListener(worldDisplay);
			
			JScrollPane paletteScroll = new JScrollPane(palette);
			paletteScroll.setPreferredSize(new Dimension(TILE_SIZE*6, TILE_SIZE*WorldDisplay.SCREENH));
			panel.add(paletteScroll);
			
			
		panel.add(worldDisplay);
		
		frame.pack();
		frame.setVisible(true);
		frame.repaint();
	}
	
}
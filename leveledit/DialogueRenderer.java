import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class DialogueRenderer {


	private static class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			if (actionEvent.getSource() == renderButton) {
				renderText();
			}
		}
	}

	private static class TextRenderComponent extends Component {
		private Image osc;
		private BufferedImage[] fontImages;
		public static final int WIDTH = 90;
		public static final int HEIGHT = 20;
		
		public TextRenderComponent() {
			this.fontImages = new BufferedImage[CPCOUNT];
			this.osc = new BufferedImage(WIDTH * CHARW, HEIGHT * CHARH, 1);
			for (int i = 0; i < CPCOUNT; ++i) {
				try {
					this.fontImages[i] = ImageIO.read(new File(MORLEQUARIPATH + "/img/font/" + i + ".png"));
				} catch (IOException ex) {
					
				}
			}
		}
		
		@Override
		public Dimension getPreferredSize() {
			return new Dimension(WIDTH * CHARW, HEIGHT * CHARH);
		}
		
		public void renderText(String s) {
			Graphics graphics = this.osc.getGraphics();
			graphics.setColor(new Color(0x58,0x58,0x58));
			graphics.fillRect(0, 0, WIDTH * CHARW, HEIGHT * CHARH);
			int codepage = 0;
			int writePos = 0;
			int lines = 0;
			for(int i=0; i < s.length(); ++i) {
				if (s.charAt(i) == '\\') {
					++i;
					switch (s.charAt(i)) {
						case 'R': codepage = 1; break;
						case 'B': codepage = 2; break;
						case 'S': codepage = 3; break;
						case 'c': codepage = 0; break;
						case 'n': {
							++lines;
							writePos = 0;
							break;
						}
					}
				} else if (s.charAt(i) == '\n') {
					++lines;
					writePos = 0;
				} else {
					graphics.drawImage(
						this.fontImages[codepage].getSubimage(
							((s.charAt(i) - ' ') % 12) * CHARW,
							((s.charAt(i) - ' ') / 12) * CHARH,
							CHARW, CHARH
						),
						(writePos % WIDTH) * CHARW,
						(lines + writePos / WIDTH) * CHARH,
						null
					);
					++writePos;
				}
			}
		}
		
		@Override
		public void paint(Graphics graphics) {
			graphics.drawImage(this.osc, 0, 0, null);
		}
	}

    public static final String MORLEQUARIPATH = "..\\..\\..\\Morlequariat";
    public static final int CHARW = 12;
    public static final int CHARH = 20;
    public static final int CPCOUNT = 4;
    private static JFrame frame;
    private static JPanel sidePanel;
    private static JTextArea input;
    private static JButton renderButton;
    private static TextRenderComponent renderComponent;
    
    public static void renderText() {
        renderComponent.renderText(input.getText());
        renderComponent.repaint();
    }
    
    public static void main(String[] args) {
        frame = new JFrame("MorlequariEdit");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, 1));
		frame.setContentPane(panel);
        
		sidePanel = new JPanel();
			input = new JTextArea(10, 100);
			sidePanel.add(input);
			
			renderButton = new JButton("Render");
			renderButton.addActionListener(new ButtonListener());			
			sidePanel.add(renderButton);	
		panel.add(sidePanel);
	
		renderComponent = new TextRenderComponent();
		panel.add(renderComponent);
		frame.pack();
		frame.setVisible(true);
		frame.repaint();
    }
}


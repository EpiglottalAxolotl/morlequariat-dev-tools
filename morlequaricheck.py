
class Header:
  def __init__(self):
    self.width = 0;
    self.height = 0;
    self.tileset = 0;
    self.music = 0;
    self.dungeon = 0;

def validateHeader(line):
  numbers = line.split()
  if len(numbers) != 5:
    print("Error: level header must contain exactly five numbers, but got", line)
    return
  for i in numbers:
    if not i.isnumeric():
      print("Error: non-numeric value in level header:", i);
  # To do: validate that numbers[0] * numbers[1] = size of tiles and images files
  # To do: validate that numbers[2] names a tileset
  # To do: validate that numbers[3] names a music
    header = Header()
    header.width, header.height, header.tileset, header.music, header.dungeon = map(int, numbers)
    return header
    
def validateCustomTerrain(tokens, header):
  print("Warning: custom terrain validation is not implemented")

def validateItem(tokens, header):
  if len(tokens) != 3:
    print("Error: item definition must contain exactly three numbers, but got", len(tokens))
    return
  if not (tokens[0].isnumeric() and tokens[1].isnumeric()):
    print("Error: item definition must contain numeric coordinates, but got (%s, %s)" % tokens[0], tokens[1])
    return
  x, y = map(int, tokens[:2])
  if x > header.width or y > header.height:
    print("Error: item at (%s, %s) is out of bounds" % tokens[0], tokens[1])
    return
  # To do: validate that tokens[2] names an item

def validateWeapon(tokens, header):
  if len(tokens) != 3:
    print("Error: weapon definition must contain exactly three numbers, but got", len(tokens))
    return
  if not (tokens[0].isnumeric() and tokens[1].isnumeric()):
    print("Error: weapon definition must contain numeric coordinates, but got (%s, %s)" % tokens[0], tokens[1])
    return
  x, y = map(int, tokens[:2])
  if x > header.width or y > header.height:
    print("Error: weapon at (%s, %s) is out of bounds" % tokens[0], tokens[1])
    return
  # To do: validate that tokens[2] names a weapon
  
def validateEnemy(tokens, header):
  if len(tokens) != 4:
    print("Error: enemy definition must contain exactly four numbers, but got", len(tokens))
    return
  if not (tokens[0].isnumeric() and tokens[1].isnumeric()):
    print("Error: enemy definition must contain numeric coordinates, but got (%s, %s)" % tokens[0], tokens[1])
    return
  x, y = map(int, tokens[:2])
  if x > header.width or y > header.height:
    print("Error: enemy at (%s, %s) is out of bounds" % tokens[0], tokens[1])
    return
  # To do: validate that tokens[2] names an enemy
  # To do: validate that tokens[3] is negative or names an item

def validateCustomTerrain(tokens, header):
  print("Warning: custom terrain validation is not implemented")
    
def validateNpc(tokens, header):
  if len(tokens) != 4:
    print("Error: NPC definition must contain exactly three numbers, but got", len(tokens))
    return
  if not (tokens[0].isnumeric() and tokens[1].isnumeric()):
    print("Error: NPC definition must contain numeric coordinates, but got (%s, %s)" % tokens[0], tokens[1])
    return
  x, y = map(int, tokens[:2])
  if x > header.width or y > header.height:
    print("Error: NPC at (%s, %s) is out of bounds" % tokens[0], tokens[1])
    return
  # To do: validate that tokens[2] is a valid npc id
  # To do: validate that tokens[3] is in-bounds for the level's dialogue
    
def validateDoor(tokens, header):
  if len(tokens) != 5:
    print("Error: door definition must contain exactly five numbers, but got", len(tokens))
    return
  if not (tokens[0].isnumeric() and tokens[1].isnumeric()):
    print("Error: door definition must contain numeric coordinates, but got (%s, %s)" % tokens[0], tokens[1])
    return
  x, y = map(int, tokens[:2])
  if x > header.width or y > header.height:
    print("Error: door at (%s, %s) is out of bounds" % tokens[0], tokens[1])
    return
  # To do: validate that tokens[2] is a valid room id
  # To do: validate that (tokens[3], tokens[4]) is in-bounds for the destination room

def validateTrigger(tokens, header):
  #print("Trigger validation is not yet supported")
  pass

def validateLookText(tokens, header):
  if len(tokens) != 3:
    print("Error: on-look text definition must contain exactly three numbers, but got", len(tokens))
    return
  if not (tokens[0].isnumeric() and tokens[1].isnumeric()):
    print("Error: on-look text definition must contain numeric coordinates, but got (%s, %s)" % tokens[0], tokens[1])
    return
  x, y = map(int, tokens[:2])
  if x > header.width or y > header.height:
    print("Error: on-look text at (%s, %s) is out of bounds" % tokens[0], tokens[1])
    return
  # To do: validate that tokens[2] is in-bounds for the level's dialogue
  
def validateCheckpoint(tokens, header):
  if len(tokens) != 3:
    print("Error: checkpoint definition must contain exactly three numbers, but got", len(tokens))
    return
  if not (tokens[0].isnumeric() and tokens[1].isnumeric()):
    print("Error: checkpoint definition must contain numeric coordinates, but got (%s, %s)" % tokens[0], tokens[1])
    return
  x, y = map(int, tokens[:2])
  if x > header.width or y > header.height:
    print("Error: checkpoint at (%s, %s) is out of bounds" % tokens[0], tokens[1])
    return
  charge = int(tokens[2])
  if charge < 0 or charge >= 8:
    print("Error: checkpoint properties are out of bounds; expected an integer in the range 0-7 but got %s" % tokens[2])
  

validationMethods = {
  'T': validateCustomTerrain,
  'i': validateItem,
  'w': validateWeapon,
  'e': validateEnemy,
  'n': validateNpc,
  'd': validateDoor,
  't': validateTrigger,
  'l': validateLookText,
  'c': validateCheckpoint
}
    
def validateLine(line, header):
  tokens = line.split()
  if tokens[0] in validationMethods:
    validationMethods[tokens[0]](tokens[1:], header)
  else:
    print("Warning: unrecognized line:", line)

def validateDataFile(filename):
  with open(filename) as thefile:
    print("Validating "+filename)
    lines = [line for line in thefile]
    header = validateHeader(lines[0])
    if header:
      for line in lines[1:]:
        validateLine(line, header)
    else:
      print("Malformed header for "+filename)
    
MORLEQUARIPATH = "../../Morlequariat"
DATA = "/map/data/"
    
if __name__ == "__main__":
  from os import listdir
  for filename in listdir(MORLEQUARIPATH + DATA):
    validateDataFile(MORLEQUARIPATH + DATA + filename)
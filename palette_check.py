from PIL import Image
from os import listdir
from colorsys import rgb_to_hsv
import itertools


def getColorCounts():
  colors_dict = {}

  # Count pixels by color and file
  for name in filter(lambda x: x.endswith(".png"), listdir("../../Morlequariat/img")):
    qualname = "../../Morlequariat/img/"+name
    img = Image.open(qualname)
    for i in range(img.width):
      for j in range(img.height):
        pix = img.getpixel((i,j))
        if len(pix)==3 or pix[3]: # alpha channel
          pix = pix[:3]
          if pix not in colors_dict:
            colors_dict[pix] = {qualname:1}
          elif qualname not in colors_dict[pix]:
            colors_dict[pix][qualname] = 1
          else:
            colors_dict[pix][qualname] += 1

  return colors_dict
# Sort pixels by nearest-neighbor
# wait never mind that's np complete (euclidean tsp with integer coordinates)

if __name__ == "__main__":

  output = open("../palette_list.html",'w')
  output.write("<html><body>")

  template = '<div style="background-color:#%s%s%s">(%d,%d,%d): %s</div>'

  def hsvKey(rgb):
    return rgb_to_hsv(*rgb)

  def someOrder(list):
    return sorted(list, key=hsvKey)

  hex2 = lambda x: "%02x" % x

  colors_dict = getColorCounts()

  for color in someOrder(colors_dict.keys()):
    output.write(template % (hex2(color[0]),hex2(color[1]),hex2(color[2]), color[0], color[1], color[2], '; '.join("File %s (%d pixels)" % (key, colors_dict[color][key]) for key in colors_dict[color])))
    
  output.write("</body></html>")

  output.close();

  threshold = 512

  output = open("../palette_collisions.html", "w")
  output.write("<html><body>")
  template = '<div><span style="background-color:#%s%s%s; display: inline-block; width:50px">&nbsp;</span><span style="background-color:#%s%s%s;display: inline-block;width:50px">&nbsp;</span>(%d,%d,%d) vs. (%d,%d,%d) (square-distance %d)</div>'


  for pair in sorted(filter(lambda i:i[1] < threshold, map(lambda a : (a, sum((x-y)*(x-y) for x,y in zip(*a))), itertools.combinations(colors_dict.keys(), 2))), key=lambda i:i[1]):
    output.write(template % (hex2(pair[0][0][0]),hex2(pair[0][0][1]),hex2(pair[0][0][2]), hex2(pair[0][1][0]),hex2(pair[0][1][1]),hex2(pair[0][1][2]), pair[0][0][0], pair[0][0][1], pair[0][0][2], pair[0][1][0], pair[0][1][1], pair[0][1][2], pair[1]))
  output.write("</body></html>")
  
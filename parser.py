import os
from sys import argv

buttonWidth = 47

def assertMinTokenCount(tokens, count, index):
  if len(tokens) < count:
    syntax_error(index, "Too few arguments to command (expected at least " + str(count) + ")")

def insert_breaks(tokens, maxlength):
  count = -1; # account for the leading space that is added
  output = ""
  for token in tokens:
    # All escape sequences used in dialogue should be 2 chars
    # Use >= to account for space being added
    tokenLength = len(token) - 2*token.count('\\')
    if count + tokenLength >= maxlength:
      output += "\\n"
      count = 0
    else:
      output += " "
      count += 1

    output += token
    count += tokenLength

  return output.strip()

def parse_dialogue(majchar, minchar, words):
  return "d" + majchar + "\t" + minchar + "\t" + insert_breaks(words, 48 if int(majchar) >= 0 else 54)

gotoLookup = {
  'room': 'F',
  'global': 'G',
  'item': 'I'
}

def parse_goto(index, tokens, labels):
  if tokens[0] not in labels:
    raise ValueError("Bad semantics on line " + str(index + 1) + ": Label " + tokens[0] + " has not been declared")
  if len(tokens) == 1:
    return "j" + str(labels[tokens[0]])
  if len(tokens) != 4 or tokens[1] != "if" :
    syntax_error(index, 'Malformed goto (expected "goto LABEL if CONDITION PARAM")')
  if tokens[2] not in gotoLookup:
    syntax_error(index, 'Unknown goto condition ' + tokens[2])
    
  return gotoLookup[tokens[2]] + tokens[3] + '\t' + str(labels[tokens[0]])

doLookup = {
  'sound':    ('s', 1),
  'music':    ('m', 1),
  'image':    ('i', 1),
  'check':    ('c', 1),
  'remove':   ('r', 1),
  'add_rf':   ('A', 1),
  'add_gf':   ('a', 1),
  'rem_gf':   ('R', 1),
  'hide_npc': ('h', 1),
  'item':     ('g', 1),
  'trigger':  ('t', 2),
  'center':   ('c', 2),
  'friend':   ('@', 2),
  'put_npc':  ('P', 4),
  'npc_walk': ('n', 3)
}

def syntax_error(index, problem):
  raise ValueError("Bad syntax on line " + str(index + 1) + ": " + problem)

def parse_do(index, tokens):
  char, params = doLookup[tokens[0]]
  if len(tokens) != params + 1:
    syntax_error(index, "Expected " + str(params) + " parameters but got " + str(len(tokens) - 1))
  return char + '\t'.join(tokens[1:])

# Commands handled by the preprocessor, which should not be included in line counts or compiled
_notsloc = {
  'label',
  '#',
  'scriptfile',
  'mapfile'
}

def parse_lines(lines):
  parsedLines = []

  labels = dict()
  
  sloc = 0
  inbranch = False
  
  outFileName = None
  
  # Preprocessing: get list of labels and other such metadata
  for i in range(len(lines)):
    tokens = lines[i].strip().split()
    if len(tokens) == 0:
      continue
    elif tokens[0] =='label':
      if len(tokens) == 2:
        labels[tokens[1]] = sloc
      else:
        syntax_error(i, "Invalid label declaration")
    elif tokens[0] == 'scriptfile':
        outFileName = "script/" + tokens[1]
    elif tokens[0] == 'mapfile':
        outFileName = "map/script/" + tokens[1]
    elif tokens[0] == 'branch':
        inbranch = True
    elif tokens[0] == 'endbranch':
        inbranch = False
          
    if not inbranch and not tokens[0] in _notsloc:
      sloc += 1
  
  print("Active labels:")
  print(labels)
  
  majchar = None
  minchar = None
  
  i = 0
  while i < len(lines):
    tokens = lines[i].strip().split()
    
    if len(tokens) == 0:
      i += 1
      continue
    
    elif tokens[0] == 'branch':
      assertMinTokenCount(tokens, 3, i)
      branchStartIndex = i;
      majchar = tokens[1]
      minchar = tokens[2]
      bLine = "b" + majchar + "\t" + minchar + "\t" + insert_breaks(tokens[3:], 48 if int(majchar) >= 0 else 54) + "\t"
      
      i += 1
      branches = []
      
      while lines[i].strip() != "endbranch":
        branchTokens = lines[i].strip().split()
        if len(branchTokens) == 0:
          i += 1
          continue
        if branchTokens[0] != 'goto' or len(branchTokens) < 4 or branchTokens[2] != 'if':
          syntax_error(i, "Expected branch goto or endbranch")
        branchText = " ".join(branchTokens[3:])
        if len(branchText) - 2*branchText.count('\\') > buttonWidth:
          print("Warning: Option button text on line " + str(i) + " is too long")
        branches.append(str(labels[branchTokens[1]]) + '\t' + branchText)
        i += 1
        if i >= len(lines):
          raise ValueError("Unexpected EOF while parsing branch on line " + str(i))
          
      if len(branches) == 0:
        syntax_error(branchStartIndex, "Branch statement has no branches")
        
      bLine += str(len(branches)) + "\t" + "\t".join(branches)
      parsedLines.append(bLine)
    
    elif tokens[0] == ':':
      assertMinTokenCount(tokens, 3, i)
      majchar = tokens[1]
      minchar = tokens[2]
      parsedLines.append(parse_dialogue(majchar, minchar, tokens[3:]))
    
    elif tokens[0] == ',':
      if not majchar:
        raise ValueError("Bad semantics on line " + str(index + 1) + ": Cannot reuse previous speaker as no speaker has been declared")
      parsedLines.append(parse_dialogue(majchar, minchar, tokens[1:]))
    
    elif tokens[0] == '.':
      parsedLines.append(parse_dialogue("-2", "-1", tokens[1:]))
      
    elif tokens[0] == 'goto':
      parsedLines.append(parse_goto(i, tokens[1:], labels))
      
    elif tokens[0] == 'do':
      parsedLines.append(parse_do(i, tokens[1:]))
      
    elif tokens[0] == 'end':
      parsedLines.append('e')
      
    elif tokens[0] == 'addrandom':
      assertMinTokenCount(tokens, 2, i)
      parsedLines.append('!' + str(labels[tokens[1]]))
      
    elif tokens[0] == 'gorandom':
      parsedLines.append('?')

    elif tokens[0] not in _notsloc:
      syntax_error(i, "Unknown command starting with " + tokens[0])
      
    i += 1
      
  return parsedLines, outFileName
  
def compile_script(inputPathRoot, inputName, outputPathRoot):
  with open(os.path.join(inputPathRoot, inputName)) as inputFile:
    lines = [line for line in inputFile]
    
  parsedLines, outFileName = parse_lines(lines)
  
  outputPath = os.path.join(outputPathRoot, outFileName or inputName)
  
  with open(outputPath, 'w') as outputFile:
    for line in parsedLines:
      print(line, file=outputFile)
      
def compile_all(inputPathRoot, outputPathRoot):
  for file in os.scandir(inputPathRoot):
    if file.is_dir():
      compile_all(os.path.join(inputPathRoot, file.name), outputPathRoot)
    else:
      compile_script(inputPathRoot, file.name, outputPathRoot)
    
morlequaripath = "../../Morlequariat"

if __name__ == "__main__":
  if len(argv) > 1:
    for filename in argv[1:]:
      compile_script(os.path.join(morlequaripath, "src", "script"), filename, morlequaripath)
  else:
    compile_all(os.path.join(morlequaripath, "src", "script"), morlequaripath)